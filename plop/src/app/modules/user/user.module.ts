import { NgModule } from "@angular/core";

import { UserRoutingModule } from "./user-routing.module";
import { SettingsComponent } from "./pages/settings/settings.component";
import { HomeComponent } from "./pages/home/home.component";
import { SharedModule } from "src/app/shared/shared.module";

@NgModule({
  declarations: [SettingsComponent, HomeComponent],
  imports: [SharedModule, UserRoutingModule]
})
export class UserModule {}
