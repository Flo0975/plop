import { Component, OnInit } from "@angular/core";
import { isCommand, getCommand } from "src/app/core/utils/commands";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  constructor() {}

  ngOnInit() {}

  test(event: any) {
    const value = event.target.value;
    const result = "Helllo";
    if (isCommand(value)) {
      const command = getCommand(value);
      if(command !== ""){
        console.log(result);
      }
    }
  }
}
