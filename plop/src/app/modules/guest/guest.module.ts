import { NgModule } from "@angular/core";

import { GuestRoutingModule } from "./guest-routing.module";
import { LoginComponent } from "./pages/login/login.component";
import { RegisterComponent } from "./pages/register/register.component";
import { ResetComponent } from "./pages/reset/reset.component";
import { SharedModule } from "src/app/shared/shared.module";

@NgModule({
  declarations: [LoginComponent, RegisterComponent, ResetComponent],
  imports: [SharedModule, GuestRoutingModule]
})
export class GuestModule {}
