import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { GuestLayoutComponent } from "./layouts/guest-layout/guest-layout.component";
import { UserLayoutComponent } from "./layouts/user-layout/user-layout.component";
import { AdminLayoutComponent } from "./layouts/admin-layout/admin-layout.component";

const routes: Routes = [
  {
    component: GuestLayoutComponent,
    path: "",
    loadChildren: () =>
      import("./modules/guest/guest.module").then((module) => module.GuestModule)
  },
  {
    component: UserLayoutComponent,
    path: "user",
    loadChildren: () =>
      import("./modules/user/user.module").then((module) => module.UserModule)
  },
  {
    component: AdminLayoutComponent,
    path: "admin",
    loadChildren: () =>
      import("./modules/admin/admin.module").then((module) => module.AdminModule)
  },
  {
    path: "**",
    redirectTo: "",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
