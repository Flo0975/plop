import { NgModule } from "@angular/core";
import { UserFooterComponent } from "./user-footer/user-footer.component";
import { UserHeaderComponent } from "./user-header/user-header.component";
import { UserLayoutComponent } from "./user-layout.component";
import { SharedModule } from "src/app/shared/shared.module";

@NgModule({
  declarations: [UserFooterComponent, UserHeaderComponent, UserLayoutComponent],
  imports: [SharedModule],
  exports: [UserLayoutComponent]
})
export class UserLayoutModule {}
