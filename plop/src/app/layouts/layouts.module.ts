import { NgModule } from "@angular/core";
import { GuestLayoutModule } from "./guest-layout/guest-layout.module";
import { AdminLayoutModule } from "./admin-layout/admin-layout.module";
import { UserLayoutModule } from "./user-layout/user-layout.module";

@NgModule({
  declarations: [],
  imports: [AdminLayoutModule, GuestLayoutModule, UserLayoutModule],
  exports: [AdminLayoutModule, GuestLayoutModule, UserLayoutModule]
})
export class LayoutsModule {}
