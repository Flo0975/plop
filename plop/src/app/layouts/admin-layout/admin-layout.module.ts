import { NgModule } from "@angular/core";
import { AdminHeaderComponent } from "./admin-header/admin-header.component";
import { AdminFooterComponent } from "./admin-footer/admin-footer.component";
import { AdminLayoutComponent } from "./admin-layout.component";
import { SharedModule } from "src/app/shared/shared.module";

@NgModule({
  declarations: [
    AdminHeaderComponent,
    AdminFooterComponent,
    AdminLayoutComponent
  ],
  imports: [SharedModule],
  exports: [AdminLayoutComponent]
})
export class AdminLayoutModule {}
