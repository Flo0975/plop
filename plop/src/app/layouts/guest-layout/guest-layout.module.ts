import { NgModule } from "@angular/core";
import { GuestFooterComponent } from "./guest-footer/guest-footer.component";
import { GuestHeaderComponent } from "./guest-header/guest-header.component";
import { GuestLayoutComponent } from "./guest-layout.component";
import { SharedModule } from "src/app/shared/shared.module";

@NgModule({
  declarations: [
    GuestFooterComponent,
    GuestHeaderComponent,
    GuestLayoutComponent
  ],
  imports: [SharedModule],
  exports: [GuestLayoutComponent]
})
export class GuestLayoutModule {}
