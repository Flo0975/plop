import { NgModule } from "@angular/core";
import { SharedModule } from "../shared/shared.module";
import { BrowserModule } from "@angular/platform-browser";
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

@NgModule({
  declarations: [],
  imports: [SharedModule],
  exports: [BrowserModule, HttpClientModule, BrowserAnimationsModule]
})
export class CoreModule {}
