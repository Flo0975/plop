export const isCommand = (value: string): boolean => {
  return value[0] === "!";
};

export const getCommand = (value: string): string => {
  const commands = ["plop", "thomas"];
  let result: string = "";

  commands.forEach(c => {
    if (value.includes(c)) {
      result = c;
      return;
    }
  });

  return result;
};
