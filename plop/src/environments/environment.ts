// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyCb0DzZ4MJr8WbrbNNZzr8tyl4F8MYoUuw",
    authDomain: "plop-4187d.firebaseapp.com",
    databaseURL: "https://plop-4187d.firebaseio.com",
    projectId: "plop-4187d",
    storageBucket: "plop-4187d.appspot.com",
    messagingSenderId: "527370481661",
    appId: "1:527370481661:web:09dcf89d2972ed6654b394"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
